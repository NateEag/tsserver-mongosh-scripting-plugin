# tsserver-mongosh-scripting-plugin

I've been using [MongoDB](https://www.mongodb.com/) at my day job for quite a
while now.

I've gotten tired of using the GUI tools for it, and have been wishing I could
get good autocompletion while writing
[mongosh](https://www.mongodb.com/docs/mongodb-shell/) scripts in Emacs.

I may see how to build such a thing without too much effort.

## Basic Design

### Mongosh-Level Completions + Other Code Intelligence

[`tsserver`](https://github.com/microsoft/TypeScript/wiki/Standalone-Server-(tsserver))
supports [behavior modification through
plugins](https://github.com/microsoft/TypeScript/wiki/Writing-a-Language-Service-Plugin).

It seems feasible to get decent code intelligence in mongosh scripts by writing
a `tsserver` plugin that just adds the mongosh globals to the `tsserver`
instance, using the existing TS types from the `mongosh` and/or `mongodb` npm
packages.

You could get code completion more quickly by just hacking in
[`@mongosh/autocomplete`](https://www.npmjs.com/package/@mongosh/autocomplete),
but then you wouldn't get the free intelligence you otherwise would from
`tsserver`, like auto-docs on hover over methods and the like.

The hardest part would just be figuring out what types to declare for the
mongosh globals, and it wouldn't be that hard - the [API
reference](https://www.mongodb.com/docs/mongodb-shell/reference/methods/)
should make it easy to hunt things down (e.g., in about five minutes I found
good evidence that [this Database
class](https://github.com/mongodb-js/mongosh/blob/main/packages/shell-api/src/database.ts)
is the type for the `db` global).

### Collection Field Inference

Once the above's working, you could move on to inferring collection schemas, so
you could get decent autocompletion on available fieldnames / types while
writing queries.

If the plugin could get a connection to the target MongoDB instance, it could
[extract any validation JSON Schemas from the
collections](https://www.mongodb.com/docs/manual/core/schema-validation/view-existing-validation-rules/)
and use those JSON Schemas to supply type information in query functions based
on the targeted collection name. When no schema exists, it could fall back
to pulling the most recent document from the collection and using its available
properties.

Perhaps more interesting, even without a MongoDB connection, if the plugin can
find any [Mongoose schemas](https://mongoosejs.com/docs/guide.html) in the
current file's project (a.k.a. "git checkout"), it should be feasible to use
those schemas to determine collection names and the document schemas they map
to.

There may well be complications I'm missing.

It seems like it could be worth trying to hack out a POC, though.
